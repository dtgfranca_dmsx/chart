import { Component, ViewChild } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import chartJs from 'chart.js';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	tipo: string;
	
  constructor(public navCtrl: NavController) {

  }

  ngAfterViewInit(){
		this.tipo ='chegada'
  }

}
