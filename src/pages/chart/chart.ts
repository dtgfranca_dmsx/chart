import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import chartJs from 'chart.js';
import { elementAttribute } from '@angular/core/src/render3/instructions';


@IonicPage()
@Component({
  selector: 'page-chart',
  templateUrl: 'chart.html',
})
export class ChartPage {
  @ViewChild('barCanvas') barCanvas;

  barChat: any;
  lineChart: any;
  pieChart: any;
  totalBar: number = 3;
  testeMes=[];
  dataAtual:Date =new Date();
  valorReceita: number =10.8;
  valorDespesa: number = 4;
  resultado = Math.round(this.valorReceita -this.valorDespesa);

//simula dos dados vindo do banco de dados
meses= [
        {mes: '1/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '2/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '3/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '4/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '5/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '6/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '7/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '8/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '9/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '10/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '11/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
        {mes: '12/2018', valores:[{receita:this.valorReceita}, {despesa:this.valorDespesa},{resultado:this.resultado}]},
    ];

//retorna o data montado do grafico
adicionarDataSet(data?){ 

     let array =[];
     let receita=[]
     let resultado=[]
     let despesa =[]
     data.forEach(element => {
         array.push(element.valores)
         console.log(element)
         if(typeof element.mes !='undefined')
            this.testeMes.push(element.mes)
     });
    
     array.forEach(element => {
         for(var i =0; i< element.length;i++){
 
             if(typeof element[i].receita !='undefined'){
                 receita.push(element[i].receita)         
              }
             
             if(typeof element[i].despesa !='undefined'){
                 despesa.push(element[i].despesa)         
             }
 
             if(typeof element[i].resultado !='undefined'){
                 resultado.push(element[i].resultado)         
             } 
         }
         
     }); 
    
     return {
    
      labels:this.testeMes,
      datasets:[{
        label:["Receita"],			
        data: receita,
        backgroundColor:'rgb(103, 187, 251)'
      },{
        label:["Despesa"],			
        data:despesa,			
        backgroundColor:'rgb(200, 6, 0)'
        
      },
      {
        label:["Resultado"],			
        data:resultado,
        backgroundColor: 'rgb(30, 29, 102)'
    
      },   
    
    
    ]    }
    
  }


  constructor(
      public navCtrl: 
      NavController, public navParams: NavParams, 
      private modalCrtl: ModalController) {  }

  ionViewDidLoad() {
    
   
    let mes = this.dataAtual.getMonth() + 1;
    let ano = this.dataAtual.getFullYear();
    let mes_ano = mes+"/"+ano;
    
     let arrayDados = this.meses.filter(function(elem, index,array){
        if(elem.mes == mes_ano){
            return [elem];
        }
    })
    
    this.barChat  = this.getBarChart(this.adicionarDataSet(arrayDados));
    
   
   
  }

 /*
 * FUNÇÃO QUE CRIA OS GRÁFICOS
 *parametro
 * context = pega o elemento canvas do html
 * charType = tipo do gráfico que pode ser bar, pie ou line
 *  data = dados do gráfico
 * options = é opcional o preenciment. Nela vc configura posiçoes da legenda
 *           tamanho das barras, os elementos X e Y
 * */
  getChart(context, charType, data, options?){
      
	  return new chartJs(context,{
		  data,
		  options,
		  type:charType

	  })
  }

  /*
  * FUNÇÃO PARA GERAR O GRÁFICO DE BARRAS
  * O parâmetro dados pega os valores arrays que vem da
  * consulta com o banco
  */

  getBarChart(dados){
	 
	  const options ={
			            legend: { display: true, position:'bottom' },
                        title: {
                                display: true,
                                text: 'Despesas'
                            },
		                scales: {
                                    xAxes: [{
							                    ticks:{
								                        beginAtZero:true
                                                    },
                                                    
                                            }],
                                    yAxes: [{
							                    ticks:{
								                    beginAtZero:true
							                     },
                                            }]
                                }
	  }
	  return this.getChart(this.barCanvas.nativeElement,'bar', dados,options);
  }
 
  /*
  * FUNÇÃO PARA ABRIR O MODAL DE PESQUISA
  */
  openFilter(){
    
    let modal = this.modalCrtl.create('ModalPage');

    modal.onDidDismiss(data=>{        
        let date = JSON.parse(JSON.stringify(data));        
        let dataPesquisa = new Date(date.dataPesquisa);
      
        this.searchDespesas(dataPesquisa,this.meses)
    })

    modal.present();

  }

  convertDateFormAmerican(date){
      
      let data = date.split('/');     
      let dataFormatada = new Date(data[1]+'-'+ this.pad(data[0],2));
      
      return dataFormatada;
  }

  searchDespesas(dataPesquisa, dados){   
    dados = dados.filter((elem, index,array)=>{
       
        if(this.convertDateFormAmerican(elem.mes).getMonth()+1 == dataPesquisa.getMonth()+1 && this.convertDateFormAmerican(elem.mes).getFullYear() ==  dataPesquisa.getFullYear())          
            return [elem];
        
    });
    //Limpa os dados anteriores no gráfico
    this.barChat.data.datasets="";
    this.barChat.data.labels.pop()
    this.barChat.update();
    
    //atualiza com os novos dados ao gráfico
    this.barChat.data = this.adicionarDataSet(dados)
    this.barChat.update();
  

  }
/*
* FUNÇÃO PARA ADICIONAR ZEROS À ESQUERDA
*/
  pad(num:number, size:number): string {           
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
  



}
